<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Application
 *
 * @ORM\Table(name="application")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="take_date", type="string")
     */
    private $take_date;

    /**
     * @var string
     *
     * @ORM\Column(name="assume_back_date", type="string")
     */
    private $assume_back_date;

    /**
     * @var string
     *
     * @ORM\Column(name="back_date", type="string", nullable=true)
     */
    private $back_date;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", inversedBy="applications")
     */
    private $book;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Reader", inversedBy="applications")
     */
    private $reader;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set takeDate
     *
     * @param string $takeDate
     *
     * @return Application
     */
    public function setTakeDate($takeDate)
    {
        $this->take_date = $takeDate;

        return $this;
    }

    /**
     * Get takeDate
     *
     * @return string
     */
    public function getTakeDate()
    {
        return $this->take_date;
    }

    /**
     * Set assumeBackDate
     *
     * @param string $assumeBackDate
     *
     * @return Application
     */
    public function setAssumeBackDate($assumeBackDate)
    {
        $this->assume_back_date = $assumeBackDate;

        return $this;
    }

    /**
     * Get assumeBackDate
     *
     * @return string
     */
    public function getAssumeBackDate()
    {
        return $this->assume_back_date;
    }

    /**
     * @param $backDate
     * @return $this
     */
    public function setBackDate($backDate)
    {
        $this->back_date = $backDate;

        return $this;
    }

    /**
     * Get backDate
     *
     * @return string
     */
    public function getBackDate()
    {
        return $this->back_date;
    }

    /**
     * Set book
     *
     * @param \AppBundle\Entity\Book $book
     *
     * @return Application
     */
    public function setBook(\AppBundle\Entity\Book $book = null)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return \AppBundle\Entity\Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set reader
     *
     * @param \AppBundle\Entity\Reader $reader
     *
     * @return Application
     */
    public function setReader(\AppBundle\Entity\Reader $reader = null)
    {
        $this->reader = $reader;

        return $this;
    }

    /**
     * Get reader
     *
     * @return \AppBundle\Entity\Reader
     */
    public function getReader()
    {
        return $this->reader;
    }
}
