<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="string")
     */
    private $adress;

    /**
     * @var string
     *
     * @ORM\Column(name="pasport", type="string", unique=true)
     */
    private $pasport;

    /**
     * @var string
     *
     * @ORM\Column(name="library_card", type="string")
     */
    private $library_card;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application", mappedBy="reader")
     */
    private $applications;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->applications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Reader
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set adress
     *
     * @param string $adress
     *
     * @return Reader
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * Set pasport
     *
     * @param string $pasport
     *
     * @return Reader
     */
    public function setPasport($pasport)
    {
        $this->pasport = $pasport;

        return $this;
    }

    /**
     * Get pasport
     *
     * @return string
     */
    public function getPasport()
    {
        return $this->pasport;
    }

    /**
     * Set libraryCard
     *
     * @param string $libraryCard
     *
     * @return Reader
     */
    public function setLibraryCard($libraryCard)
    {
        $this->library_card = $libraryCard;

        return $this;
    }

    /**
     * Get libraryCard
     *
     * @return string
     */
    public function getLibraryCard()
    {
        return $this->library_card;
    }

    /**
     * Add application
     *
     * @param \AppBundle\Entity\Application $application
     *
     * @return Reader
     */
    public function addApplication(\AppBundle\Entity\Application $application)
    {
        $this->applications[] = $application;

        return $this;
    }

    /**
     * Remove application
     *
     * @param \AppBundle\Entity\Application $application
     */
    public function removeApplication(\AppBundle\Entity\Application $application)
    {
        $this->applications->removeElement($application);
    }

    /**
     * Get applications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplications()
    {
        return $this->applications;
    }
}
