<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Application;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadApplicationData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $reader1 = $this->getReference('reader1');
        $reader2 = $this->getReference('reader2');
        $reader3 = $this->getReference('reader3');

        $book1 = $this->getReference('book_1');
        $book2 = $this->getReference('book_2');
        $book3 = $this->getReference('book_3');



        $application = new Application();
        $application
            ->setReader($reader3)
            ->setBook($book1)
            ->setTakeDate('2018-01-01 00:00:00')
            ->setAssumeBackDate('2018-02-01 00:00:00')
            ->setBackDate('2018-01-31 00:00:00');

        $manager->persist($application);

        $application = new Application();
        $application
            ->setReader($reader1)
            ->setBook($book1)
            ->setTakeDate('2018-03-01 00:00:00')
            ->setAssumeBackDate('2018-08-01 00:00:00');

        $manager->persist($application);

        $application = new Application();
        $application
            ->setReader($reader1)
            ->setBook($book2)
            ->setTakeDate('2018-03-01 00:00:00')
            ->setAssumeBackDate('2018-08-01 00:00:00');

        $manager->persist($application);

        $application = new Application();
        $application
            ->setReader($reader2)
            ->setBook($book3)
            ->setTakeDate('2018-03-01 00:00:00')
            ->setAssumeBackDate('2018-04-01 00:00:00')
            ->setBackDate('2018-03-30 00:00:00');

        $manager->persist($application);

        $manager->persist($application);

        $application = new Application();
        $application
            ->setReader($reader3)
            ->setBook($book3)
            ->setTakeDate('2018-05-01 00:00:00')
            ->setAssumeBackDate('2018-06-01 00:00:00');

        $manager->persist($application);


        $manager->flush();
    }



    public function getDependencies()

    {

        return array(

            LoadReaderData::class,
            LoadBookData::class

        );

    }
}
