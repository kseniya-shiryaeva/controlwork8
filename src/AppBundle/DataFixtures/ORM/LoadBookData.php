<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBookData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category1 = $this->getReference('category1');
        $category2 = $this->getReference('category2');
        $category3 = $this->getReference('category3');
        $category4 = $this->getReference('category4');
        $category5 = $this->getReference('category5');

        $books = [
            ['title' => 'Мастер и Маргарита', 'author' => 'Михаил Афанасьевич Булгаков', 'foto' => 'w136h203-72d701af.jpg', 'categories' => [$category1, $category2]],
            ['title' => 'Евгений Онегин', 'author' => 'Александр Сергеевич Пушкин', 'foto' => 'w136h203-790df161.jpg', 'categories' => [$category1]],
            ['title' => 'Преступление и наказание', 'author' => 'Федор Михайлович Достоевский', 'foto' => 'w136h203-461a7a0b.jpg', 'categories' => [$category1]],
            ['title' => 'Война и мир', 'author' => 'Лев Толстой', 'foto' => 'w136h203-acf50da0.jpg', 'categories' => [$category1]],
            ['title' => 'Двенадцать стульев', 'author' => 'Евгений Петров, Илья Ильф', 'foto' => 'w136h203-f359ea02.jpg', 'categories' => [$category1]],
            ['title' => 'Гарри Поттер и философский камень', 'author' => 'Джоан К. Роулинг', 'foto' => 'w136h203-7c720ed0.jpg', 'categories' => [$category2, $category4]],
            ['title' => 'Властелин Колец', 'author' => 'Джон Рональд Руэл Толкин', 'foto' => 'w136h203-ff740f01.jpg', 'categories' => [$category2]],
            ['title' => 'Шерлок Холмс', 'author' => 'Артур Конан Дойл', 'foto' => 'w136h203-1ccef2c9.jpg', 'categories' => [$category1, $category5]],
            ['title' => 'Ревизор', 'author' => 'Николай Васильевич Гоголь', 'foto' => 'w136h203-4168ff5c.jpg', 'categories' => [$category1, $category3]],
            ['title' => 'Ромео и Джульетта', 'author' => 'Уильям Шекспир', 'foto' => 'w136h203-63f64095.jpg', 'categories' => [$category1, $category3]],
            ['title' => 'Незнайка на Луне', 'author' => 'Николай Носов', 'foto' => 'w136h203-26a2d944.jpg', 'categories' => [$category2, $category4]],
            ['title' => 'Понедельник начинается в субботу', 'author' => 'Аркадий Стругацкий, Борис Стругацкий', 'foto' => 'w136h203-9b36894c.jpg', 'categories' => [$category2]],
            ['title' => 'Волшебник Изумрудного города', 'author' => 'Александр Мелентьевич Волков', 'foto' => 'w136h203-85c26e75.jpg', 'categories' => [$category2, $category4]],
            ['title' => 'Трудно быть богом', 'author' => 'Аркадий Стругацкий, Борис Стругацкий', 'foto' => 'w136h203-d821bff3.jpg', 'categories' => [$category2]],
            ['title' => 'Пикник на обочине', 'author' => 'Аркадий Стругацкий, Борис Стругацкий', 'foto' => 'w136h203-b9afb8aa.jpg', 'categories' => [$category2]]
        ];

        foreach ($books as $key => $one_book){
            $book = new Book();
            $book
                ->setTitle($one_book['title'])
                ->setAuthor($one_book['author'])
                ->setFoto($one_book['foto']);
            foreach ($one_book['categories'] as $one_category){
                $book->addCategory($one_category);
            }
            $this->addReference('book_'.$key, $book);

            $manager->persist($book);
        }


        $manager->flush();
    }



    public function getDependencies()

    {

        return array(

            LoadCategoryData::class,

        );

    }
}
