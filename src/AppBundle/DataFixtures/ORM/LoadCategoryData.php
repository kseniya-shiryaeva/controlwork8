<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1
            ->setTitle('Classic');

        $manager->persist($category1);
        $this->addReference('category1', $category1);

        $category2 = new Category();
        $category2
            ->setTitle('Fantastic');

        $manager->persist($category2);
        $this->addReference('category2', $category2);


        $category3 = new Category();
        $category3
            ->setTitle('Drama');

        $manager->persist($category3);
        $this->addReference('category3', $category3);


        $category4 = new Category();
        $category4
            ->setTitle('Children');

        $manager->persist($category4);
        $this->addReference('category4', $category4);


        $category5 = new Category();
        $category5
            ->setTitle('Detective');

        $manager->persist($category5);
        $this->addReference('category5', $category5);

        $manager->flush();
    }
}
