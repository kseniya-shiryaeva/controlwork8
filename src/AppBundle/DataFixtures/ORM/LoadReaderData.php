<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Reader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadReaderData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $reader1 = new Reader();
        $reader1
            ->setName('Reader1')
            ->setAdress('Bishkek')
            ->setPasport('AN1111')
            ->setLibraryCard('cardAN1111')
        ;

        $manager->persist($reader1);

        $this->addReference('reader1', $reader1);

        $reader2 = new Reader();
        $reader2
            ->setName('Reader2')
            ->setAdress('Moscow')
            ->setPasport('AN2222')
            ->setLibraryCard('cardAN1111')
        ;

        $manager->persist($reader2);

        $this->addReference('reader2', $reader2);

        $reader3 = new Reader();
        $reader3
            ->setName('Reader3')
            ->setAdress('London')
            ->setPasport('AN3333')
            ->setLibraryCard('cardAN1111')
        ;

        $manager->persist($reader3);

        $this->addReference('reader3', $reader3);

        $manager->flush();
    }
}
