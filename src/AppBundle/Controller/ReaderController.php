<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reader;
use AppBundle\Form\ReaderType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/reader")
 */
class ReaderController extends Controller
{
    /**
     * @Route("/new")
     * @Method({"GET","HEAD","POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(ReaderType::class, new Reader(), array('method' => 'POST'));

        $form->handleRequest($request);
        $reader = $form->getData();
        $reader->setLibraryCard('card'.$reader->getPasport());

        if($form->isSubmitted() && $form->isValid()) {

            $em->persist($reader);
            $em->flush($reader);

            return $this->redirectToRoute("app_book_index");
        }

        return $this->render('@App/Reader/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
