<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Application;
use AppBundle\Entity\Book;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BookController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $books = $this->getDoctrine()->getRepository('AppBundle:Book')->findAll();


        return $this->render('@App/Book/index.html.twig', array(
            'categories' => $categories,
            'books' => $books,
            'category_id' => false,
            'status' => $this->getStatus($books)
        ));
    }

    /**
     * @Route("/category/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(int $id)
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $one_category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);

        $books = $one_category->getBooks();

        return $this->render('@App/Book/index.html.twig', array(
            'categories' => $categories,
            'books' => $books,
            'category_id' => $id,
            'status' => $this->getStatus($books)
        ));
    }

    /**
     * @param Book[] $books
     * @return array
     */
    private function getStatus($books){
        $status = [];

        foreach ($books as $key => $book){
            $status[$key]['value'] = true;
            $status[$key]['message'] = '';

            /**
             * @var Application[]
             */
            $applications = $book->getApplications();
            if(count($applications) > 0){
                foreach ($applications as $application){
                    if(!$application->getBackDate()){
                        $status[$key]['value'] = false;
                        $status[$key]['message'] = 'Ожидается к '.date('d/m/Y', strtotime($application->getAssumeBackDate()));
                    }
                }
            }
        }

        return $status;
    }

    /**
     * @Route("/book/{id}/take", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function takeBookAction($id){

        $book = $this->getDoctrine()->getRepository('AppBundle:Book')->find($id);

        $form_builder = $this
            ->createFormBuilder()
            ->add('reader', TextType::class)
            ->add('assume_back_date', DateType::class, array(
                'widget' => 'choice',
            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Take book'
            ]);
        $form = $form_builder->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            $application = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($application);
            $em->flush();

            return $this->redirectToRoute("app_book_index");

        }

        return $this->render('@App/Book/take_book.html.twig', array(
            'book' => $book,
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/reader/{id}/books", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function readerBooksAction(int $id)
    {
        $one_reader = $this->getDoctrine()->getRepository('AppBundle:Reader')->find($id);

        /**
         * @var Application[]
         */
        $applications = $one_reader->getApplications();

        $books =[];

        foreach ($applications as $application){
            $books[] = $application->getBook();
        }

        return $this->render('@App/Book/index.html.twig', array(
            'books' => $books
        ));
    }


}
